default[:logstash][:version] = "1.4"
default[:logstash][:crt_folder] = '/etc/pki/tls/certs'
default[:logstash][:key_folder] = '/etc/pki/tls/private'
default[:logstash][:crt_file] = '/etc/pki/tls/certs/logstash.crt'
default[:logstash][:key_file] = '/etc/pki/tls/private/logstash.key'
